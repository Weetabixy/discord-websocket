var WebSocketServer = require('websocket').server;
require("dotenv").config();

var fs = require("fs")
var server = require('http').createServer(function (request, response) {
    fs.readFile('index.html', function (err, data) {
        if (!err) {
            response.writeHead(200, {'Content-Type':'text/html'})
            response.write(data)
            response.end()
        } else {
            console.error(err)
        }
    })
});
var discord = require("discord.js");
var client = new discord.Client();
var channel;

server.listen(8080, function () { console.log("Server Listening On Port 8080") });

wsServer = new WebSocketServer({
    httpServer: server
});

function sendMsg(msg, con) {
    con.send('IMSG{"name": "' + msg.author.username + '", "icon":"' + msg.author.displayAvatarURL + '", "content":"' + msg.content + '","channel":"' + msg.channel.name + '" } ');
}

wsServer.on('request', function (request) {
    var connection = request.accept(null, request.origin);

    client.on("message", (msg) => {
        if (msg.content == ".set") {
            console.log("Channel has been set");
            channel = msg.channel;
           
            

        } else {
            sendMsg(msg, connection);
        }
    })

    connection.on('message', function (message) {
        if (message.type === 'utf8') {
            if (message.utf8Data.substring(4, 0) == "DMSG") {
                if (channel == null) {
                    console.log("no channel set?")
                }  else {
                    channel.send(message.utf8Data.substring(4, message.utf8Data.length));
                }
            } else if (message.utf8Data.substring(4, 0) == "CSET") {
                console.log("chanel change req")
                channel = client.guilds.first().channels.find("name", message.utf8Data.substring(4, message.utf8Data.length));              
            }
        }
    });

    connection.on("close", function (reason) {
        console.log("Client Left For: " + reason)
        connection = null;
    })
});

client.login(process.env.TOKEN).catch((err) => {
    console.error("An incorrect key or no key was supplied");
    server.close()
});